package com.atlassian.util.concurrent.bootcamp;

import static com.google.common.base.Predicates.in;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.junit.Test;

import com.atlassian.fugue.Option;
import com.atlassian.util.concurrent.RuntimeInterruptedException;
import com.google.common.base.Function;

public class StackMultiThreadTest {

  @Test public void multiplePoppersCantRetrieveDuplicatesWhenNonePresent() throws InterruptedException {
    final Stack<Integer> stack = Stack.of();
    int count = 10000;
    for (int i = 0; i < count; ++i) {
      stack.push(i);
    }

    class Popper implements Runnable {
      final List<Integer> popped = new ArrayList<Integer>();

      @Override public void run() {
        while (true) {
          Option<Integer> pop = stack.pop();
          if (pop.isDefined())
            popped.add(pop.get());
          else
            break;
        }
      }
    }
    Popper p1 = new Popper();
    Popper p2 = new Popper();

    runTest(p1, p2);

    // You should get out what you put in!
    assertThat(p1.popped.size() + p2.popped.size(), is(equalTo(count)));

    // The input stack has distinct elements - there is no way for the popped
    // elements to overlap
    assertThat(any(p1.popped, in(p2.popped)), is(false));
    assertThat(any(p2.popped, in(p1.popped)), is(false));
  }

  @Test public void multiplePushersAllHaveTheirEntriesRecorded() throws InterruptedException {
    int size = 10000;
    final Stack<Integer> stack = Stack.of();

    class Pusher implements Runnable {
      final List<Integer> integerRange;

      Pusher(List<Integer> integerRange) {
        this.integerRange = integerRange;
      }

      @Override public void run() {
        for (Integer i : integerRange) {
          stack.push(i);
        }
      }
    }

    Pusher p1 = new Pusher(makeRange(0, size / 2));
    Pusher p2 = new Pusher(makeRange(size / 2, size));

    runTest(p1, p2);

    List<Integer> content = new ArrayList<Integer>();
    while (stack.peek().isDefined()) {
      content.add(stack.pop().get());
    }

    // If we've got two threads pushing 500 each, we should end with 1000
    // entries!
    assertThat(content.size(), is(equalTo(size)));

    // If we've got two threads simultaneously pushing each half we
    // should end up with the full caboodle
    assertThat(content, is(containsInAnyOrder(makeRange(0, size).toArray())));
  }

  @Test public void multiplePushersAndPoppers() throws InterruptedException {
    final int size = 10000;
    final Stack<Integer> stack = Stack.of();

    class Pusher implements Runnable {
      final List<Integer> range;

      Pusher(List<Integer> range) {
        this.range = range;
      }

      @Override public void run() {
        for (Integer i : range) {
          stack.push(i);
        }
      }
    }

    class Popper implements Runnable {
      final List<Integer> is = new ArrayList<Integer>();
      int popped = 0;

      @Override public void run() {
        while (popped < size) {
          Option<Integer> pop = stack.pop();
          if (pop.isDefined()) {
            is.add(pop.get());
          }
          popped += 1;
        }
      }
    }
    Pusher push1 = new Pusher(makeRange(0, size / 2));
    Pusher push2 = new Pusher(makeRange(size / 2, size));
    Popper pop1 = new Popper();
    Popper pop2 = new Popper();
    Popper pop3 = new Popper();
    Popper pop4 = new Popper();

    assertThat(push1.range.size() + push2.range.size(), is(size));

    runTest(push1, push2, pop1, pop2, pop3, pop4);

    final Iterable<Integer> content = concat(pop1.is, pop2.is, pop3.is, pop4.is);
    assertThat(size(content), is(equalTo(size)));
    assertThat(content, is(containsInAnyOrder(makeRange(0, size).toArray())));
  }

  static void runTest(Runnable... rs) {
    final Latch wait = new Latch(rs.length);

    // start threads all awaiting the latch, need to copy to array as transform
    // is lazy and doesn't memoize
    Thread[] threads = toArray(transform(Arrays.asList(rs), new Function<Runnable, Thread>() {
      @Override public Thread apply(final Runnable run) {
        final Thread thread = new Thread(new Runnable() {
          @Override public final void run() {
            wait.ready();
            run.run();
          }
        });
        thread.start();
        return thread;
      }
    }), Thread.class);
    for (Thread thread : threads) {
      thread.checkAccess();
    }
    wait.go();
    try {
      for (Thread thread : threads) {
        thread.join();
      }
    } catch (InterruptedException e) {
      throw new RuntimeInterruptedException(e);
    }
  }

  private List<Integer> makeRange(int closed, int open) {
    List<Integer> res = new ArrayList<Integer>(open - closed);
    for (int i = closed; i < open; ++i) {
      res.add(i);
    }
    return res;
  }

  /** basic ready, set, go style latch */
  static class Latch {
    final CountDownLatch ready;
    final CountDownLatch go = new CountDownLatch(1);

    Latch(int numberOfThreads) {
      ready = new CountDownLatch(numberOfThreads);
    }

    void ready() {
      ready.countDown();
      try {
        go.await();
      } catch (InterruptedException e) {
        throw new RuntimeInterruptedException(e);
      }
    }

    void go() {
      try {
        ready.await();
      } catch (InterruptedException e) {
        throw new RuntimeInterruptedException(e);
      }
      go.countDown();
    }
  }
}
