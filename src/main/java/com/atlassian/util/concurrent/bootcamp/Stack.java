package com.atlassian.util.concurrent.bootcamp;

import java.util.Iterator;

import com.atlassian.fugue.Option;

/**
 * Simple singly-linked Stack template.
 */
public class Stack<A> implements Iterable<A> {
  /**
   * Construct a Stack whose elements are made up of the supplied elements,
   * where the left-most – or first – elements are the ones that will be first
   * out. For instance <code>Stack.&lt;Integer&gt; of(1, 2, 3).peek()</code>
   * will result in <code>1</code>.
   */
  static <A> Stack<A> of(@SuppressWarnings("unchecked") A... as) {
    throw new UnsupportedOperationException();
  }

  @Override public Iterator<A> iterator() {
    throw new UnsupportedOperationException();
  }

  /**
   * non-destructively look at the head element, if there is one
   */
  public Option<A> peek() {
    throw new UnsupportedOperationException();
  }

  /**
   * pop off the head element, if there is one
   */
  public Option<A> pop() {
    throw new UnsupportedOperationException();
  }

  /**
   * push a new head element
   * 
   * @returns this
   */
  public Stack<A> push(A a) {
    throw new UnsupportedOperationException();
  }
}
