# Concurrency tests

Exercise 1

implement a Stack using the supplied specification. Firstly, do it without regard to thread-safety. Secondly consider and then implement the same thing but with a thread-safe, but lock-free manner. 
