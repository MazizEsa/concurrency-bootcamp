package com.atlassian.util.concurrent.bootcamp;

import static com.atlassian.fugue.Option.some;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterableOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Iterator;

import org.junit.Test;

import com.atlassian.fugue.Option;

public class StackTest {
  @Test public void emptyStack() {
    assertThat(Stack.<String> of(), is(emptyIterableOf(String.class)));
  }

  @Test public void emptyStackPeekReturnsNone() {
    assertThat(Stack.<String> of().peek(), is(Option.<String> none()));
  }

  @Test public void emptyStackPopReturnsNone() {
    assertThat(Stack.<String> of().pop(), is(Option.<String> none()));
  }

  @Test public void emptyStackPopTwice() {
    Stack<String> empty = Stack.of();
    assertThat(empty.pop(), is(Option.<String> none()));
    assertThat(empty.pop(), is(Option.<String> none()));
  }

  @Test public void emptyStackPushContainsItem() {
    assertThat(Stack.<Integer> of().push(3), is(contains(3)));
  }

  @Test public void emptyStackPushPopStillEmpty() {
    Stack<String> s = Stack.<String> of();
    s.push("hi").pop();
    assertThat(s, is(emptyIterableOf(String.class)));
  }

  @Test public void singleStackPeekReturnsSome() {
    assertThat(Stack.of(1).peek(), is(some(1)));
  }

  @Test public void singleStackPopReturnsSome() {
    assertThat(Stack.of(1).pop(), is(some(1)));
  }

  @Test public void singleStackPeekStillNotEmpty() {
    Stack<Integer> s = Stack.of(1);
    s.peek();
    assertThat(s, is(contains(1)));
  }

  @Test public void singleStackPopNowEmpty() {
    Stack<String> s = Stack.of("1");
    s.pop();
    assertThat(s, is(emptyIterableOf(String.class)));
  }

  @Test public void singleStackPushContainsItem() {
    assertThat(Stack.of(2).push(3), is(contains(3, 2)));
  }

  @Test public void singleStackPushPopIsUnchanged() {
    Stack<Integer> s = Stack.of(9);
    s.push(8).pop();
    assertThat(s, is(contains(9)));
  }

  @Test public void multiStackPeeksHead() {
    assertThat(Stack.of(1, 2, 3).peek(), is(some(1)));
  }

  @Test public void multiStackPopsHead() {
    assertThat(Stack.of(1, 2, 3).pop(), is(some(1)));
  }

  @Test public void multiStackPopChangesStack() {
    Stack<Integer> s = Stack.of(1, 2, 3);
    s.pop();
    assertThat(s, is(contains(2, 3)));
  }

  @Test public void multiStackFactoryIsCorrectOrder() {
    assertThat(Stack.of(1, 2, 3), is(contains(1, 2, 3)));
  }

  @Test public void multiStackPushesToHead() {
    assertThat(Stack.of(2, 3).push(1), is(contains(1, 2, 3)));
  }

  @Test public void multiStackPushPopStillEmpty() {
    Stack<Integer> s = Stack.of(7, 8, 9);
    s.push(8).pop();
    assertThat(s, is(contains(7, 8, 9)));
  }

  @Test public void iteratorNextBeforePushIsStable() {
    Stack<Integer> s = Stack.of(1);
    Iterator<Integer> it = s.iterator();
    it.next();
    s.push(2);
    assertThat(it.hasNext(), is(false));
  }

  @Test public void iteratorPushBeforeNextIsStable() {
    Stack<Integer> s = Stack.of(1);
    Iterator<Integer> it = s.iterator();
    s.push(2);
    it.next();
    assertThat(it.hasNext(), is(false));
  }

  @Test(expected = NullPointerException.class) public void ofNull() {
    Stack.of((String) null);
  }

  @Test(expected = NullPointerException.class) public void pushNull() {
    Stack.of().push(null);
  }
}
